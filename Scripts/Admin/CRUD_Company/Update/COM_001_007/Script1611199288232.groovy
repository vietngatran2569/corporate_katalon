import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/Page_AdminCorporate/lbl_Company'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_AdminCorporate/Page_Corporate/btn_Action'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_AdminCorporate/Page_Corporate/btn_Edit'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Name_CompanyProfile'), 'Lotus Solution')

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_AdminCorporate/Page_Corporate/btn_Update'), 2)

WebUI.delay(2)

WebUI.click(findTestObject('Page_AdminCorporate/Page_Corporate/btn_Update'))

WebUI.mouseOver(findTestObject('Object Repository/err_message_css'))

WebUI.verifyMatch(WebUI.getText(findTestObject('Object Repository/err_message_css')).trim(), 'Succeed', false)

