import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Page_AdminCorporate/btn_Company'))

WebUI.click(findTestObject('Object Repository/Page_AdminCorporate/btn_ Create'))

WebUI.setText(findTestObject('Page_AdminCorporate/input_Name_CompanyProfile'), nameCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Phone_CompanyProfile'), phoneCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Address_CompanyProfile'), addressCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_ContactPhone_CompanyProfile'), contactPhoneCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_ContactPosition_CompanyProfile'), contactPositionCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Prefix_CompanyProfile'), prefixCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_ShortName_CompanyProfile'), shortnameCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Email_CompanyProfile'), emailCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Description_CompanyProfile'), descriptionCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_ContactName_CompanyProfile'), contactNameCompanyProfile)

WebUI.setText(findTestObject('Page_AdminCorporate/input_ContactEmail_CompanyProfile'), contactEmailCompanyProfile)

WebUI.delay(1)

CustomKeywords.'help.UploadFile.uploadFile'(findTestObject('Page_AdminCorporate/input_img_Logo'), logoImage)

WebUI.delay(2)

WebUI.click(findTestObject('Page_AdminCorporate/btnCrop'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_AdminCorporate/input_Name_Admin'), 2)

WebUI.delay(2)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Name_Admin'), nameAdministrator)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Email_Admin'), emailAdministrator)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Password_Admin'), passwordAdministrator)

WebUI.setText(findTestObject('Page_AdminCorporate/input_Phone_Admin'), phoneAdministrator)

WebUI.setText(findTestObject('Page_AdminCorporate/input_UserName_Admin'), usernameAdministrator)

WebUI.setText(findTestObject('Page_AdminCorporate/input_RetypePassword_Admin'), retypePasswordAdministrator)

//GroupForm groupForm=new GroupForm()
//WebUI.click(groupForm.getItemDropdownListByName('Corporate 2'))
WebUI.mouseOver(findTestObject('Page_AdminCorporate/div_Select_Corporate'))

//WebUI.click(findTestObject('Page_AdminCorporate/optionCorporate'))
WebUI.click(findTestObject('Page_AdminCorporate/button_Create'))

WebUI.mouseOver(findTestObject('Object Repository/err_message_css'))

WebUI.verifyMatch(WebUI.getText(findTestObject('Object Repository/err_message_css')).trim(), expect_rs, false)

WebUI.refresh()

WebUI.delay(1)

WebUI.acceptAlert()

WebUI.delay(1)

