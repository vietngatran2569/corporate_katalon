import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_Company/My Task/menu_MyTask'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Company/My Task/btn_detailTask'))

WebUI.scrollToElement(findTestObject('Page_Company/My Task/btn_approve'), 100)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Company/My Task/btn_approve'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Page_Company/My Task/verify_approved'), 'approved')

WebUI.click(findTestObject('Page_Company/My Task/btn_detailTask'))

actual_row1 = WebUI.getText(findTestObject('Page_Company/My Task/Validate_accountInvalid/validateStatus_row1'))

WebUI.verifyEqual(actual_row1, expect_row1)

actual_row2 = WebUI.getText(findTestObject('Page_Company/My Task/Validate_accountInvalid/validateStatus_row2'))

WebUI.verifyEqual(actual_row2, expect_row2)

actual_row3 = WebUI.getText(findTestObject('Page_Company/My Task/Validate_accountInvalid/validateStatus_row3'))

WebUI.verifyEqual(actual_row2, expect_row3)

