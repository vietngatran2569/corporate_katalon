<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_img_Logo</name>
   <tag></tag>
   <elementGuidId>417983fe-9dc3-4f7f-8ba7-db4c855d4c88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@class=&quot;file-picker-thumbnail&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>img.file-picker-thumbnail</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>file-picker-thumbnail</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;header-fixed sidebar-lg-show sidebar-fixed aside-menu-fixed aside-menu-off-canvas modal-open&quot;]/div[3]/div[1]/div[@class=&quot;modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/form[1]/fieldset[@class=&quot;fieldset-form&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6 col-sm-6 col-md-6&quot;]/div[@class=&quot;row form-group&quot;]/div[@class=&quot;col-md-8&quot;]/img[@class=&quot;file-picker-thumbnail&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/img</value>
   </webElementXpaths>
</WebElementEntity>
