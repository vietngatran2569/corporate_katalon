<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create</name>
   <tag></tag>
   <elementGuidId>f010113d-e42c-4c18-9343-fd126ec2b7e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;modal-footer&quot;]/button[@type=&quot;button&quot; and @class=&quot;mr-1 btn btn-success&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-footer&quot;]/button[@type=&quot;button&quot; and @class=&quot;mr-1 btn btn-success&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-footer > button.mr-1.btn.btn-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-footer&quot;]/button[@type=&quot;button&quot; and @class=&quot;mr-1 btn btn-success&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[49]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select...'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/button</value>
   </webElementXpaths>
</WebElementEntity>
