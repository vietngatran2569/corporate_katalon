<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_approve</name>
   <tag></tag>
   <elementGuidId>f1106700-cde9-4e4c-aae1-d9e42319453f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot;btn btn-success mr-2 btn btn-secondary&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot;btn btn-success mr-2 btn btn-secondary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
